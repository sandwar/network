﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;

namespace SandWar.Network.TestUnit
{
	[TestFixture]
	public class LidgrenTests
	{
		public class MyClass {
			public long Test;
		}

		
		IServer<object> server;
		IClient<object> client;

		[SetUp]
		public void LidgrenInitialize() {
			server = LidgrenNetwork.CreateServer<object>(29573);
			client = LidgrenNetwork.CreateClient<object>(29573);
		}

		[TearDown]
		public void LidgrenCleanup() {
			if(client.Status != ConnectionStatus.Disconnected)
				client.Disconnect();
			
			if(server.IsOpen)
				server.Close();
			
			server = null;
			client = null;
			
			Thread.Sleep(250);
		}



		[Test]
		public void ServerToClientMessageTest() {
			bool messageRead = false;
			client.OnMessageIncoming = (message) => {
				messageRead = true;

				int first = message.ReadInt();
				Assert.AreEqual(123, first, "Read integer");

				string second = message.ReadString();
				Assert.AreEqual("test", second, "Read string");

				MyClass third = message.Read<MyClass>();
				Assert.AreEqual(12345, third.Test, "Read object content");
			};

			var peer = simulateConnection();

			var msg = server.CreateMessage();
			msg.Write(123);
			msg.Write("test");
			msg.Write(new MyClass() {
				Test = 12345
			});

			peer.SendMessage(msg, true, true);

			Thread.Sleep(250);

			Assert.IsTrue(messageRead, "Message read");
		}

		[Test]
		public void ClientToServerMessageTest() {
			bool messageRead = false;
			IPeer<object> messageReadFrom = null;
			server.OnMessageIncoming = (peer, message) => {
				messageRead = true;

				int first = message.ReadInt();
				Assert.AreEqual(456, first, "Read integer");

				string second = message.ReadString();
				Assert.AreEqual("test2", second, "Read string");

				messageReadFrom = peer;
			};

			var myPeer = simulateConnection();

			var msg = server.CreateMessage();
			msg.Write(456);
			msg.Write("test2");

			client.SendMessage(msg, true, true);

			Thread.Sleep(250);

			Assert.IsTrue(messageRead, "Message read");
			Assert.AreEqual(myPeer, messageReadFrom, "The peer that sent the message");
		}

		[Test]
		public void DisconnectionTest() {
			bool disconnectDetected = false;
			IPeer<object> disconnectedPeer = null;
			server.OnDisconnect = (peer) => {
				disconnectDetected = true;
				disconnectedPeer = peer;
			};

			var myPeer = simulateConnection();

			client.Disconnect();

			Thread.Sleep(250);

			Assert.IsTrue(disconnectDetected, "Disconnection detected");
			Assert.AreEqual(myPeer, disconnectedPeer, "The disconnected peer");
		}

		[Test]
		public void RejectConnectionTest() {
			bool disconnectDetected = false;
			client.OnDisconnect = () => {
				disconnectDetected = true;
			};

			simulateConnection(false);

			Thread.Sleep(250);

			Assert.IsTrue(disconnectDetected, "Disconnection detected");
		}



		private IPeer<object> simulateConnection(bool acceptConnection = true) {
			IPeer<object> thePeer = null;
			server.OnConnect = (peer) => {
				thePeer = peer;
				return acceptConnection;
			};

			server.OpenAsync().Start();
			Thread.Sleep(1000);

			Assert.IsTrue(server.IsOpen, "Connection open");

			client.ConnectAsync("localhost").Start();
			Thread.Sleep(1000);

			if(acceptConnection) {
				Assert.AreEqual(ConnectionStatus.Connected, client.Status, "Client status");
				Assert.AreEqual(1, server.ConnectedPeers.Count(), "Number of connected peers");
				Assert.AreEqual(thePeer, server.ConnectedPeers.First(), "The connected peer");
			} else {
				Assert.AreEqual(client.Status, ConnectionStatus.Disconnected, "Client status");
				Assert.AreEqual(server.ConnectedPeers.Count(), 0, "Number of connected peers");
			}

			return thePeer;
		}
	}
}
