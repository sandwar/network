﻿using System;
using SandWar.Tools.Threading.Tasks;

namespace SandWar.Network
{
	/// <summary>Represents a client.</summary>
	public interface IClient<T>
	{
		/// <summary>The connection status between this client and a server.</summary>
		ConnectionStatus Status { get; }
		
		/// <summary>Approximate time a packet takes to reach the opposite end of the connection.</summary>
		int Ping { get; }
		
		/// <summary>The time the connection was estabilished.</summary>
		DateTime ConnectionTime { get; }
		
		/// <summary>Custom external data associated with this client.</summary>
		T Data { get; set; }
		
		/// <summary>Connects this client to a server.</summary>
		/// <param name="host">The address of the server to connect to.</param>
		/// <returns>A task that will complete once the client disconnects from the server.</returns>
		Task ConnectAsync(string host);
		
		/// <summary>Disconnects this client from the server.</summary>
		void Disconnect();
		
		/// <summary>Create a message to be sent throught this client.</summary>
		/// <returns>An empty message.</returns>
		IMessageOut CreateMessage();
		
		/// <summary>Send a message to the server.</summary>
		/// <param name="msg">The message.</param>
		/// <param name="ordered">Indicates wether the message should preserve the order in message queue or not.</param>
		/// <param name="reliable">Indicates wether the message must absolutely reach the server or not.</param>
		void SendMessage(IMessageOut msg, bool ordered = false, bool reliable = false);
		
		/// <summary>Called after the client successfully connects to a server.</summary>
		Action OnConnect { get; set; }
		
		/// <summary>Called then the client disconnects from the server or the connection is lost.</summary>
		Action OnDisconnect { get; set; }
		
		/// <summary>Called when a message is received from the server.</summary>
		Action<IMessageIn<T>> OnMessageIncoming { get; set; }
	}
}
