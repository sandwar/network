﻿using System;

namespace SandWar.Network
{
	/// <summary>Represents a node of a connection.</summary>
	public interface IPeer<T>
	{
		/// <summary>The connection status between this peer and a server or another peer.</summary>
		ConnectionStatus Status { get; }
		
		/// <summary>Approximate time a packet takes to reach the opposite end of the connection.</summary>
		int Ping { get; }
		
		/// <summary>The time the connection was estabilished.</summary>
		DateTime ConnectionTime { get; }

		/// <summary>Custom external data associated with this peer.</summary>
		T Data { get; set; }
		
		/// <summary>Disconnects this peer from the connected server or peer.</summary>
		void Disconnect();

		/// <summary>Send a message to the peer.</summary>
		/// <param name="msg">The message.</param>
		/// <param name="ordered">Indicates wether the message should preserve the order in message queue or not.</param>
		/// <param name="reliable">Indicates wether the message must absolutely reach the peer or not.</param>
		void SendMessage(IMessageOut msg, bool ordered = false, bool reliable = false);
	}
}
