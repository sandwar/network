﻿using System;

namespace SandWar.Network
{
	/// <summary>Represents the status of a connection between a client and a server.</summary>
	public enum ConnectionStatus
	{
		/// <summary>Unknown connection status.</summary>
		Unknown,
		
		/// <summary>Connection not estabilished yet.</summary>
		NotConnected,
		
		/// <summary>Connection in progress but not estabilished yet.</summary>
		Connecting,
		
		/// <summary>Connection estabilished and active.</summary>
		Connected,
		
		/// <summary>Connection estabilished, but being closed.</summary>
		Disconnecting,
		
		/// <summary>Connection estabilished, then closed.</summary>
		Disconnected
	}
}
