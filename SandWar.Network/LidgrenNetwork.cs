﻿using System;
using SandWar.Logging;
using SandWar.Network.Lidgren;

namespace SandWar.Network
{
	/// <summary>Allows to create lidgren-network-gen3 clients and servers.</summary>
	public static class LidgrenNetwork
	{
		/// <summary>Create a new lidgren client.</summary>
		/// <param name="port">The port the server is serving on.</param>
		/// <param name="log">The log.</param>
		/// <returns>A new Lidgren client.</returns>
		public static IClient<T> CreateClient<T>(ushort port, Log log = null) {
			return new LidgrenClient<T>(port, log);
		}
		
		/// <summary>Create a lidgren server.</summary>
		/// <param name="port">The port to serve.</param>
		/// <param name="log">The log.</param>
		/// <returns>A new Lidgren server.</returns>
		public static IServer<T> CreateServer<T>(ushort port, Log log = null) {
			return new LidgrenServer<T>(port, log);
		}
	}
}
