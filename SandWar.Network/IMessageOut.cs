﻿using System;

namespace SandWar.Network
{
	/// <summary>Represents an outgoing message.</summary>
	public interface IMessageOut
	{
		/// <summary>Write a boolean to the message.</summary>
		/// <param name="val">The boolean to write.</param>
		void Write(bool val);
		
		/// <summary>Write a byte to the message.</summary>
		/// <param name="val">The byte to write.</param>
		void Write(byte val);
		
		/// <summary>Write a signed byte to the message.</summary>
		/// <param name="val">The signed byte to write.</param>
		void Write(sbyte val);
		
		/// <summary>Write a char to the message.</summary>
		/// <param name="val">The char to write.</param>
		void Write(char val);
		
		/// <summary>Write a decimal to the message.</summary>
		/// <param name="val">The decimal to write.</param>
		void Write(decimal val);
		
		/// <summary>Write a double to the message.</summary>
		/// <param name="val">The double to write to the message.</param>
		void Write(double val);
		
		/// <summary>Write a float to the message.</summary>
		/// <param name="val">The float to write to the message.</param>
		void Write(float val);
		
		/// <summary>Write an integer to the message.</summary>
		/// <param name="val">The integer to write to the message.</param>
		void Write(int val);
		
		/// <summary>Write an unsigned integer to the message.</summary>
		/// <param name="val">The unsigned integer to write to the message.</param>
		void Write(uint val);
		
		/// <summary>Write a long to the message.</summary>
		/// <param name="val">The long to write to the message.</param>
		void Write(long val);
		
		/// <summary>Write an unsigned long to the message.</summary>
		/// <param name="val">The unsigned long to write to the message.</param>
		void Write(ulong val);
		
		/// <summary>Write a short to the message.</summary>
		/// <param name="val">The short to write to the message.</param>
		void Write(short val);
		
		/// <summary>Write an unsigned short to the message.</summary>
		/// <param name="val">The unsigned short to write to the message.</param>
		void Write(ushort val);
		
		/// <summary>Write a string to the message.</summary>
		/// <param name="val">The string to write to the message.</param>
		void Write(string val);
		
		/// <summary>Write an object to the message.</summary>
		/// <param name="val">The object to write to the message.</param>
		void Write<T>(T val);
	}
}
