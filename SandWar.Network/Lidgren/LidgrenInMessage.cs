﻿using System;
using System.IO;
using Lidgren.Network;
using SandWar.Serialization;

namespace SandWar.Network.Lidgren
{
	class LidgrenInMessage<T> : IMessageIn<T>
	{
		readonly IStreamSerializer serializer;
		
		NetIncomingMessage msg;
		MemoryStream stream;
		internal NetIncomingMessage Message {
			set {
				msg = value;
				
				if(stream != null) {
					stream.Close();
					stream = null;
				}
			}
		}
		
		internal LidgrenInMessage(IStreamSerializer serializer) {
			this.serializer = serializer;
		}
		
		public IPeer<T> Sender {
			get { return (IPeer<T>)msg.SenderConnection.Tag; }
		}
		
		public int Length {
			get { return msg.LengthBytes; }
		}
		
		public bool ReadBool() {
			return msg.ReadByte() != 0;
		}
		
		public byte ReadByte() {
			return msg.ReadByte();
		}
		
		public sbyte ReadSignedByte() {
			return msg.ReadSByte();
		}
		
		public char ReadChar() {
			return (char)msg.ReadUInt16();
		}
		
		public decimal ReadDecimal() {
			int bit1 = msg.ReadInt32();
			int bit2 = msg.ReadInt32();
			int bit3 = msg.ReadInt32();
			int bit4 = msg.ReadInt32();
			return new Decimal(new int[4] { bit1, bit2, bit3, bit4 });
		}
		
		public double ReadDouble() {
			return msg.ReadDouble();
		}
		
		public float ReadFloat() {
			return msg.ReadFloat();
		}
		
		public int ReadInt() {
			return msg.ReadInt32();
		}
		
		public uint ReadUnsignedInt() {
			return msg.ReadUInt32();
		}
		
		public long ReadLong() {
			return msg.ReadInt64();
		}
		
		public ulong ReadUnsignedLong() {
			return msg.ReadUInt64();
		}
		
		public short ReadShort() {
			return msg.ReadInt16();
		}
		
		public ushort ReadUnsignedShort() {
			return msg.ReadUInt16();
		}
		
		public string ReadString() {
			return msg.ReadString();
		}
		
		public T2 Read<T2>(T2 obj = default(T2)) {
			if(stream == null)
				stream = new MemoryStream(msg.Data, false);
			
			stream.Position = msg.PositionInBytes;
			var result = serializer.Deserialize<T2>(stream, obj);
			msg.Position = (int)stream.Position * 8;
			return result;
		}
	}
}
