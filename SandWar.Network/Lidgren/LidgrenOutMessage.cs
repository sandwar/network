﻿using System;
using System.IO;
using Lidgren.Network;
using SandWar.Serialization;

namespace SandWar.Network.Lidgren
{
	class LidgrenOutMessage : IMessageOut
	{
		readonly IStreamSerializer serializer;
		
		NetOutgoingMessage msg;
		MemoryStream stream;
		internal NetOutgoingMessage Message {
			get { return msg; }
			set {
				msg = value;
				
				if (stream == null || stream.Capacity > 8192) {
					if (stream != null)
						stream.Close();
					
					stream = new MemoryStream(4096);
				}
			}
		}
		
		internal LidgrenOutMessage(IStreamSerializer serializer) {
			this.serializer = serializer;
		}
		
		public void Write(bool val) {
			msg.Write(val ? (byte)1 : (byte)0);
		}
		
		public void Write(byte val) {
			msg.Write((byte)val);
		}
		
		public void Write(sbyte val) {
			msg.Write((sbyte)val);
		}
		
		public void Write(char val) {
			msg.Write((ushort)val);
		}
		
		public void Write(decimal val) {
			var bits = Decimal.GetBits(val);
			msg.Write((int)bits[0]);
			msg.Write((int)bits[1]);
			msg.Write((int)bits[2]);
			msg.Write((int)bits[3]);
		}
		
		public void Write(double val) {
			msg.Write((double)val);
		}
		
		public void Write(float val) {
			msg.Write((float)val);
		}
		
		public void Write(int val) {
			msg.Write((int)val);
		}
		
		public void Write(uint val) {
			msg.Write((uint)val);
		}
		
		public void Write(long val) {
			msg.Write((long)val);
		}
		
		public void Write(ulong val) {
			msg.Write((ulong)val);
		}
		
		public void Write(short val) {
			msg.Write((short)val);
		}
		
		public void Write(ushort val) {
			msg.Write((ushort)val);
		}
		
		public void Write(string val) {
			msg.Write((string)val);
		}
		
		public void Write<T>(T val) {
			stream.Position = 0;
			serializer.Serialize<T>(val, stream);
			msg.Write(stream.GetBuffer(), 0, (int)stream.Position);
		}
	}
}
