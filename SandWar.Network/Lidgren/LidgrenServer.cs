﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Lidgren.Network;
using SandWar.Logging;
using SandWar.Serialization;
using SandWar.Tools.Threading.Tasks;
using SandWar.Tools.Collections.Generic;

namespace SandWar.Network.Lidgren
{
	class LidgrenServer<T> : IServer<T>
	{
		internal const string ApplicationString = "sandboxwars_lidgren3server";
		
		readonly Log log;
		readonly NetServer server;
		readonly LidgrenMessageBuilder<T> msgBuild;
		volatile bool shutdownPlanned = true;
		
		public Func<IPeer<T>, bool> OnConnect { get; set; }
		public Action<IPeer<T>> OnConnected { get; set; }
		public Action<IPeer<T>> OnDisconnect { get; set; }
		public Action<IPeer<T>, IMessageIn<T>> OnMessageIncoming { get; set; }
		
		public LidgrenServer(int port, Log log = null) {
			OnConnect = delegate { return true; };
			OnConnected = delegate { };
			OnDisconnect = delegate { };
			OnMessageIncoming = delegate { };
			
			var config = new NetPeerConfiguration(ApplicationString) {
				Port = port
			};
			config.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);
			
			this.log = log ?? Log.Null;
			server = new NetServer(config);
			msgBuild = new LidgrenMessageBuilder<T>(new ProtobufSerializer());
		}
		
		public bool IsOpen {
			get { return !shutdownPlanned; }
		}
		
		public IEnumerable<IPeer<T>> ConnectedPeers {
			get { return server.Connections.Select((connection) => (IPeer<T>)connection.Tag); }
		}
		
		public Task OpenAsync() {
			log.Debug("Server start requested...");
			
			return new Task(() => {
				lock(server) {
					shutdownPlanned = false;
					server.Start();
				}
				
				log.Info("Server started!");
				
				while(!shutdownPlanned) {
					server.MessageReceivedEvent.WaitOne(500);
					
					NetIncomingMessage msg;
					while((msg = server.ReadMessage()) != null) {
						processMessage(msg);
						server.Recycle(msg);
					}
				}
				
				shutdown();
				
				log.Info("Server stopped!");
				
				lock (server)
					Monitor.Pulse(server);
			});
		}
		
		public void Close() {
			log.Debug("Server stop requested...");
			
			lock(server) {
				if(shutdownPlanned)
					return;
				
				shutdownPlanned = true;
				Monitor.Wait(server);
			}
		}
		
		public IMessageOut CreateMessage() {
			return msgBuild.CreateOutputMessage(server.CreateMessage());
		}
		
		public void SendMessage(IPeer<T> peer, IMessageOut msg, bool ordered = false, bool reliable = false) {
			log.DebugFormat("Message >> Count({0}) Ordered({1}) Reliable({2})", 1, ordered, reliable);
			
			NetOutgoingMessage lgmsg = ((LidgrenOutMessage)msg).Message;
			
			NetDeliveryMethod type;
			type = reliable ?
				ordered ?
					NetDeliveryMethod.ReliableOrdered :
					NetDeliveryMethod.ReliableUnordered :
				ordered ?
					NetDeliveryMethod.UnreliableSequenced :
					NetDeliveryMethod.Unreliable;
			
			server.SendMessage(lgmsg, ((LidgrenPeer<T>)peer).connection, type);
		}
		
		public void SendMessage(IEnumerable<IPeer<T>> peers, IMessageOut msg, bool ordered = false, bool reliable = false) {
			var peersList = peers.Select(peer => ((LidgrenPeer<T>)peer).connection).AsReadonlyIList();
			if (peersList.Count == 0) return;
			
			log.DebugFormat("Message >> Count({0}) Ordered({1}) Reliable({2})", peersList.Count, ordered, reliable);
			
			NetOutgoingMessage lgmsg = ((LidgrenOutMessage)msg).Message;
			
			NetDeliveryMethod type;
			type = reliable ?
				ordered ?
					NetDeliveryMethod.ReliableOrdered :
					NetDeliveryMethod.ReliableUnordered :
				ordered ?
					NetDeliveryMethod.UnreliableSequenced :
					NetDeliveryMethod.Unreliable;
			
			server.SendMessage(lgmsg, peersList, type, 0);
		}
		
		void processMessage(NetIncomingMessage msg) {
			log.DebugFormat("Message << Type({0})", msg.MessageType);
			
			switch(msg.MessageType) {
			case NetIncomingMessageType.Data:
				processDataMessage(msg);
				break;
			case NetIncomingMessageType.StatusChanged:
				processStatusChange(msg, (NetConnectionStatus)msg.ReadByte());
				break;
			case NetIncomingMessageType.ConnectionApproval:
				processConnectionRequest(msg);
				break;
			} 
		}
		
		void processConnectionRequest(NetIncomingMessage msg) {
			var peer = new LidgrenPeer<T>(this, msg.SenderConnection);
			msg.SenderConnection.Tag = peer;
			
			bool approve = false;
			try {
				log.Debug("Connecting");
				approve = OnConnect(peer);
			} catch(Exception e) {
				log.Error("processConnectionRequest", e);
			}
			
			if(approve)
				msg.SenderConnection.Approve();
			else
				msg.SenderConnection.Deny();
		}
		
		void processDataMessage(NetIncomingMessage msg) {
			var peer = (IPeer<T>)msg.SenderConnection.Tag;
			var lgmsg = msgBuild.CreateInputMessage(msg);
			
			try {
				OnMessageIncoming(peer, lgmsg);
			} catch(Exception e) {
				log.Warning("processDataMessage", e);
			}
			
			msgBuild.Recycle(lgmsg);
		}
		
		void processStatusChange(NetIncomingMessage msg, NetConnectionStatus status) {
			var peer = (IPeer<T>)msg.SenderConnection.Tag;
			if(status == NetConnectionStatus.Disconnected) {
				try {
					log.Debug("Disconnecting");
					OnDisconnect(peer);
				} catch(Exception e) {
					log.Warning("processStatusChange Disconnected exception", e);
				}
			} else if(status == NetConnectionStatus.Connected) {
				try {
					OnConnected(peer);
				} catch(Exception e) {
					log.Warning("processStatusChange Connected exception", e);
				}
			}
		}
		
		void shutdown() {
			server.Shutdown("");
		}
	}
}
