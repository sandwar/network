﻿using Lidgren.Network;
using System;

namespace SandWar.Network.Lidgren
{
	class LidgrenPeer<T> : IPeer<T>
	{
		readonly LidgrenServer<T> server;
		internal readonly NetConnection connection;
		readonly DateTime connectionTime;
		
		internal LidgrenPeer(LidgrenServer<T> server, NetConnection connection) {
			this.server = server;
			this.connection = connection;
			this.connectionTime = DateTime.UtcNow;
		}
		
		public ConnectionStatus Status {
			get {
				switch(connection.Status) {
				case NetConnectionStatus.InitiatedConnect:
				case NetConnectionStatus.ReceivedInitiation:
				case NetConnectionStatus.RespondedAwaitingApproval:
				case NetConnectionStatus.RespondedConnect:
					return ConnectionStatus.Connecting;
				
				case NetConnectionStatus.Connected:
					return ConnectionStatus.Connected;
				
				case NetConnectionStatus.Disconnecting:
					return ConnectionStatus.Disconnecting;
				
				case NetConnectionStatus.Disconnected:
					return ConnectionStatus.Disconnected;
				
				default:
					return ConnectionStatus.Unknown;
				}
			}
		}
		
		public int Ping {
			get { return (int)(connection.AverageRoundtripTime/1000); }
		}
		
		public DateTime ConnectionTime {
			get { return connectionTime; }
		}
		
		public T Data { get; set; }
		
		public void SendMessage(IMessageOut msg, bool ordered = false, bool reliable = false) {
			server.SendMessage(this, msg, ordered, reliable);
		}
		
		public void Disconnect() {
			throw new NotImplementedException();
		}
	}
}
