﻿using Lidgren.Network;
using SandWar.Serialization;
using SandWar.Tools.Collections.Concurrent;

namespace SandWar.Network.Lidgren
{
	class LidgrenMessageBuilder<T>
	{
		public const int InputPoolMaxSize = 64;
		public const int OutputPoolMaxSize = 64;
		
		readonly IStreamSerializer serializer;
		
		readonly ConcurrentBag<LidgrenInMessage<T>> inputPool;
		readonly ConcurrentBag<LidgrenOutMessage> outputPool;
		
		internal LidgrenMessageBuilder(IStreamSerializer serializer) {
			this.serializer = serializer;
		
			inputPool = new ConcurrentBag<LidgrenInMessage<T>>();
			outputPool = new ConcurrentBag<LidgrenOutMessage>();
		}
		
		public LidgrenInMessage<T> CreateInputMessage(NetIncomingMessage msg) {
			LidgrenInMessage<T> mymsg;
			if(!inputPool.TryTake(out mymsg))
				mymsg = new LidgrenInMessage<T>(serializer);
			
			mymsg.Message = msg;
			return mymsg;
		}
		
		public LidgrenOutMessage CreateOutputMessage(NetOutgoingMessage msg) {
			LidgrenOutMessage mymsg;
			if(!outputPool.TryTake(out mymsg))
				mymsg = new LidgrenOutMessage(serializer);
			
			mymsg.Message = msg;
			return mymsg;
		}
		
		public void Recycle(LidgrenInMessage<T> msg) {
			msg.Message = null;
			
			if(inputPool.Count < InputPoolMaxSize)
				inputPool.Add(msg);
		}
		
		public void Recycle(LidgrenOutMessage msg) {
			msg.Message = null;
			
			if(outputPool.Count < OutputPoolMaxSize)
				outputPool.Add(msg);
		}
	}
}
