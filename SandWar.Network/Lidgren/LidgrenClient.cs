﻿using System;
using System.Threading;
using Lidgren.Network;
using SandWar.Logging;
using SandWar.Serialization;
using SandWar.Tools.Threading.Tasks;

namespace SandWar.Network.Lidgren
{
	class LidgrenClient<T> : IClient<T>
	{
		readonly Log log;
		readonly NetClient client;
		readonly int serverPort;
		DateTime connectionTime;
		readonly LidgrenMessageBuilder<T> msgBuild;
		volatile bool shutdownPlanned = true;
		volatile bool everConnected = false;
		
		public Action OnConnect { get; set; }
		public Action OnDisconnect { get; set; }
		public Action<IMessageIn<T>> OnMessageIncoming { get; set; }
		
		public LidgrenClient(int port, Log log = null) {
			OnConnect = delegate { };
			OnDisconnect = delegate { };
			OnMessageIncoming = delegate { };
			
			var config = new NetPeerConfiguration(LidgrenServer<T>.ApplicationString);
			
			this.log = log ?? Log.Null;
			client = new NetClient(config);
			serverPort = port;
			msgBuild = new LidgrenMessageBuilder<T>(new ProtobufSerializer());
		}
		
		public Task ConnectAsync(string host) {
			log.Debug("Client start requested...");
			
			return new Task(() => {
				log.InfoFormat("Connecting to \"{0}\"...", host);
				
				lock(client) {
					shutdownPlanned = false;
					client.Start();
					client.Connect(host, serverPort);
					connectionTime = DateTime.UtcNow;
					everConnected = true;
				}

				while(!shutdownPlanned) {
					client.MessageReceivedEvent.WaitOne(500);

					NetIncomingMessage msg;
					while((msg = client.ReadMessage()) != null) {
						processMessage(msg);
						client.Recycle(msg);
					}
				}
				
				shutdown();
				
				log.Info("Client disconnected!");
				
				lock(client) {
					Monitor.Pulse(client);
				}
			});
		}
		
		public IMessageOut CreateMessage() {
			return msgBuild.CreateOutputMessage(client.CreateMessage());
		}
		
		public ConnectionStatus Status {
			get {
				if(shutdownPlanned && client.ConnectionStatus != NetConnectionStatus.Disconnected)
					return ConnectionStatus.Disconnecting;
				
				switch(client.ConnectionStatus) {
				case NetConnectionStatus.InitiatedConnect:
				case NetConnectionStatus.ReceivedInitiation:
				case NetConnectionStatus.RespondedAwaitingApproval:
				case NetConnectionStatus.RespondedConnect:
					return ConnectionStatus.Connecting;
				
				case NetConnectionStatus.Connected:
					return ConnectionStatus.Connected;
				
				case NetConnectionStatus.Disconnecting:
					return ConnectionStatus.Disconnecting;
				
				case NetConnectionStatus.Disconnected:
					if(!everConnected)
						return ConnectionStatus.NotConnected;
					return ConnectionStatus.Disconnected;
				
				default:
					return ConnectionStatus.Unknown;
				}
			}
		}
		
		public int Ping {
			get { return (int)(client.ServerConnection.AverageRoundtripTime/1000); }
		}
		
		public DateTime ConnectionTime {
			get { return connectionTime; }
		}
		
		public T Data {
			get { return (T)client.Tag; }
			set { client.Tag = value; }
		}
		
		public void SendMessage(IMessageOut msg, bool ordered = false, bool reliable = false) {
			NetOutgoingMessage lgmsg = ((LidgrenOutMessage) msg).Message;
			
			NetDeliveryMethod type;
			type = reliable ?
				ordered ?
					NetDeliveryMethod.ReliableOrdered :
					NetDeliveryMethod.ReliableUnordered :
				ordered ?
					NetDeliveryMethod.UnreliableSequenced :
					NetDeliveryMethod.Unreliable;
			
			client.SendMessage(lgmsg, type);
		}
		
		public void Disconnect() {
			log.Debug("Client disconnect requested...");
			
			lock(client) {
				if(shutdownPlanned)
					return;
				
				shutdownPlanned = true;
				Monitor.Wait(client);
			}
		}
		
		void processMessage(NetIncomingMessage msg) {
			switch(msg.MessageType) {
			case NetIncomingMessageType.Data:
				processDataMessage(msg);
				break;
			case NetIncomingMessageType.StatusChanged:
				processStatusChange(msg, (NetConnectionStatus)msg.ReadByte());
				break;
			} 
		}
		
		void processDataMessage(NetIncomingMessage msg) {
			var lgmsg = msgBuild.CreateInputMessage(msg);
			
			OnMessageIncoming(lgmsg);
			
			msgBuild.Recycle(lgmsg);
		}
		
		void processStatusChange(NetIncomingMessage msg, NetConnectionStatus status) {
			var lgmsg = msgBuild.CreateInputMessage(msg);
			
			if(status == NetConnectionStatus.Connected) {
				OnConnect();
				log.Info("...connected!");
			}
			
			if(status == NetConnectionStatus.Disconnected) {
				OnDisconnect();
				log.Info("...disconnected!");
			}
			
			msgBuild.Recycle(lgmsg);
		}
		
		void shutdown() {
			client.Disconnect("");
		}
	}
}
