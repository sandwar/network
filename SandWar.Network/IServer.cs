﻿using System;
using System.Collections.Generic;
using SandWar.Tools.Threading.Tasks;

namespace SandWar.Network
{
	/// <summary>Represents a server.</summary>
	public interface IServer<T>
	{
		/// <summary>Indicates wether this server is still hosting or not.</summary>
		bool IsOpen { get; }
		
		/// <summary>A list contanining all the connected peers.</summary>
		IEnumerable<IPeer<T>> ConnectedPeers { get; }
		
		/// <summary>Starts serving.</summary>
		/// <returns>A task that will complete once the server is closed.</returns>
		Task OpenAsync();
		
		/// <summary>Closes all active connections and stops serving.</summary>
		void Close();
		
		/// <summary>Create a message to be sent throught this server.</summary>
		/// <returns>An empty message.</returns>
		IMessageOut CreateMessage();
		
		/// <summary>Send a message to a peer.</summary>
		/// <param name="peer">The peer to send the message to.</param>
		/// <param name="msg">The message.</param>
		/// <param name="ordered">Indicates wether the message should preserve the order in message queue or not.</param>
		/// <param name="reliable">Indicates wether the message must absolutely reach the peer or not.</param>
		void SendMessage(IPeer<T> peer, IMessageOut msg, bool ordered = false, bool reliable = false);
		
		/// <summary>Send a message to a group of peers.</summary>
		/// <param name="peers">The peers to send the message to.</param>
		/// <param name="msg">The message.</param>
		/// <param name="ordered">Indicates wether the message should preserve the order in message queue or not.</param>
		/// <param name="reliable">Indicates wether the message must absolutely reach the peer or not.</param>
		void SendMessage(IEnumerable<IPeer<T>> peers, IMessageOut msg, bool ordered = false, bool reliable = false);
		
		/// <summary>Called after the client successfully connects to a server.</summary>
		Func<IPeer<T>, bool> OnConnect { get; set; }
		
		/// <summary>Called after the client successfully connects to a server and the server accepts the connection.</summary>
		Action<IPeer<T>> OnConnected { get; set; }
		
		/// <summary>Called then the client disconnects from the server or the connection is lost.</summary>
		Action<IPeer<T>> OnDisconnect { get; set; }
		
		/// <summary>Called when a message is received from a peer.</summary>
		Action<IPeer<T>, IMessageIn<T>> OnMessageIncoming { get; set; }
	}
}
