﻿using System;

namespace SandWar.Network
{
	/// <summary>Represents an incoming message.</summary>
	public interface IMessageIn<T>
	{
		/// <summary>The peer that sent the message.</summary>
		IPeer<T> Sender { get; }
		
		/// <summary>Length of the message in bytes.</summary>
		int Length { get; }
		
		/// <summary>Read a boolean from the message.</summary>
		/// <returns>The read boolean.</returns>
		bool ReadBool();
		
		/// <summary>Read a byte from the message.</summary>
		/// <returns>The read byte.</returns>
		byte ReadByte();
		
		/// <summary>Read a signed byte from the message.</summary>
		/// <returns>The read signed byte.</returns>
		sbyte ReadSignedByte();
		
		/// <summary>Read a character from the message.</summary>
		/// <returns>The read character.</returns>
		char ReadChar();
		
		/// <summary>Read a decimal from the message.</summary>
		/// <returns>The read decimal.</returns>
		decimal ReadDecimal();
		
		/// <summary>Read a double from the message.</summary>
		/// <returns>The read double.</returns>
		double ReadDouble();
		
		/// <summary>Read a float from the message.</summary>
		/// <returns>The read float.</returns>
		float ReadFloat();
		
		/// <summary>Read an integer from the message.</summary>
		/// <returns>The read integer.</returns>
		int ReadInt();
		
		/// <summary>Read an unsigned integer from the message.</summary>
		/// <returns>The read unsigned integer.</returns>
		uint ReadUnsignedInt();
		
		/// <summary>Read a long from the message.</summary>
		/// <returns>The read long.</returns>
		long ReadLong();
		
		/// <summary>Read an unsigned long from the message.</summary>
		/// <returns>The read unsigned long.</returns>
		ulong ReadUnsignedLong();
		
		/// <summary>Read a short from the message.</summary>
		/// <returns>The read short.</returns>
		short ReadShort();
		
		/// <summary>Read an unsigned short from the message.</summary>
		/// <returns>The read unsigned short.</returns>
		ushort ReadUnsignedShort();
		
		/// <summary>Read a string from the message.</summary>
		/// <returns>The read string.</returns>
		string ReadString();
		
		/// <summary>Read an object from the message.</summary>
		/// <param name="obj">The object to insert the read data into, can be null.</param>
		/// <returns><paramref name="obj"/> or a new object (if null), filled with the data.</returns>
		T2 Read<T2>(T2 obj = default(T2));
	}
}
